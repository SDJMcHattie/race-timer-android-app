package uk.co.iscoding.racetimer.realm.model;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Timing extends RealmObject {
    public interface Fields {
        String RACE = "race";
        String RUNNER = "runner";
        String FINISH_TIME = "finishTime";
    }

    @PrimaryKey @Required private String primaryKey;
    private Race race;
    private Runner runner;
    private Date startTime;
    private Date finishTime;

    public Runner getRunner() {
        return runner;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        if (startTime == null && getFinishTime() == null) {
            deleteFromRealm();
        } else {
            this.startTime = startTime;
        }
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        if (finishTime == null && getStartTime() == null) {
            deleteFromRealm();
        } else {
            this.finishTime = finishTime;
        }
    }

    public static Timing createInstance(@NonNull Race race, @NonNull Runner runner) {
        Timing timing = new Timing();
        timing.primaryKey = UUID.randomUUID().toString();
        timing.race = race;
        timing.runner = runner;

        return timing;
    }
}
