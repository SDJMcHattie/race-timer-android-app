package uk.co.iscoding.racetimer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import com.google.common.base.Preconditions;

import uk.co.iscoding.racetimer.about.AboutFragment;
import uk.co.iscoding.racetimer.base.BaseActivity;
import uk.co.iscoding.racetimer.base.NavFragment;
import uk.co.iscoding.racetimer.races.RacesFragment;
import uk.co.iscoding.racetimer.runners.RunnersFragment;

public class MainActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        FragmentManager.OnBackStackChangedListener,
        NavFragment.Listener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        getDrawer().addDrawerListener(toggle);
        toggle.syncState();

        getNavigationView().setNavigationItemSelectedListener(this);

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        if (savedInstanceState == null) {
            // Only add the initial fragment if there's no instance state
            showFragment(LaunchFragment.newInstance(), false);
            refreshTitle();
        }
    }

    @Override
    public void onBackPressed() {
        if (getDrawer().isDrawerOpen(GravityCompat.START)) {
            getDrawer().closeDrawer(GravityCompat.START);
        } else if (getTopFragment().onBackPressed()) {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        NavFragment fragment = null;

        switch (item.getItemId()) {
            case R.id.nav_runners:
                fragment = RunnersFragment.newInstance();
                break;
            case R.id.nav_races:
                fragment = RacesFragment.newInstance();
                break;
            case R.id.nav_about:
                fragment = AboutFragment.newInstance();
                break;
        }

        if (fragment != null) {
            fragment.setNavMenuItem(item);
            showFragment(fragment);
        }

        getDrawer().closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onBackStackChanged() {
        refreshTitle();

        MenuItem item = getTopFragment().getNavMenuItem();
        if (item != null) {
            item.setChecked(true);
        } else {
            // Uncheck all items in the navigation
            Menu menu = getNavigationView().getMenu();
            for (int i = 0; i < menu.size(); i++) {
                menu.getItem(i).setChecked(false);
            }
        }
    }

    private DrawerLayout getDrawer() {
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        Preconditions.checkNotNull(drawerLayout, "There should be a DrawerLayout in the view hierarchy");

        return drawerLayout;
    }

    private NavigationView getNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Preconditions.checkNotNull(navigationView, "There should be NavigationView in the view hierarchy");

        return navigationView;
    }

    private NavFragment getTopFragment() {
        return (NavFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_host);
    }

    public void showFragment(NavFragment fragment) {
        showFragment(fragment, true);
    }

    private void showFragment(NavFragment fragment, boolean addToBackStack) {
        NavFragment currentFragment = getTopFragment();
        if (currentFragment != null && fragment.getNavMenuItem() == null) {
            // Pass the nav menu item along
            fragment.setNavMenuItem(currentFragment.getNavMenuItem());
        }

        hideKeyboard();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        transaction.replace(R.id.fragment_host, fragment, null).commit();
        getSupportFragmentManager().executePendingTransactions();
    }

    private void refreshTitle() {
        setTitle(getTopFragment().getTitle());
    }

    @Override
    public void closeFragment() {
        getSupportFragmentManager().popBackStack();
        hideKeyboard();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }
}
