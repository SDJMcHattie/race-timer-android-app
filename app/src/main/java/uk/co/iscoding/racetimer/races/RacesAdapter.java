package uk.co.iscoding.racetimer.races;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.realm.model.Race;

class RacesAdapter extends RealmRecyclerViewAdapter<Race, RacesAdapter.RaceViewHolder> {
    interface ActionsListener {
        void onRaceClicked(Race race);
    }

    private final ActionsListener actionsListener;

    RacesAdapter(@NonNull Context context, @Nullable RealmResults<Race> data, @NonNull ActionsListener actionsListener) {
        super(context, data, true);
        this.actionsListener = actionsListener;
    }

    @Override
    public RaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.rv_item_race, parent, false);
        return new RaceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RaceViewHolder holder, int position) {
        Race race = getItem(position);

        if (race != null) {
            holder.bind(race, position % 2 == 1, actionsListener);
        }
    }

    static class RaceViewHolder extends RecyclerView.ViewHolder {
        private final View view;
        private final TextView name;
        private final TextView raceDate;
        private final TextView raceTime;

        RaceViewHolder(View view) {
            super(view);
            this.view = view;
            name = (TextView) view.findViewById(R.id.race_name);
            raceDate = (TextView) view.findViewById(R.id.race_date);
            raceTime = (TextView) view.findViewById(R.id.race_time);
        }

        void bind(@NonNull final Race race, boolean even, final ActionsListener actionsListener) {
            this.view.setBackground(ContextCompat.getDrawable(
                    view.getContext(),
                    even ? R.drawable.row_background_even : R.drawable.row_background_odd
            ));
            this.view.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    actionsListener.onRaceClicked(race);
                }
            });

            name.setText(race.getName());
            raceDate.setText(DateFormat.format("dd MMM yyyy", race.getPlannedDate()));
            raceTime.setText(DateFormat.format("HH:mm", race.getPlannedDate()));
        }
    }
}
