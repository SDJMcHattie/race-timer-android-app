package uk.co.iscoding.racetimer.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.common.collect.Lists;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.List;

import javax.inject.Inject;

import uk.co.iscoding.racetimer.RaceTimerApplication;

public abstract class BaseActivity extends AppCompatActivity {
    private boolean needsInjection() {
        // Gather all classes back to this base class
        Class classToAdd = getClass();
        List<Class> classes = Lists.newArrayList(classToAdd);
        while (classToAdd != Object.class) {
            classToAdd = classToAdd.getSuperclass();
            classes.add(classToAdd);
        }

        for (Class classToCheck : classes) {
            // Check all fields for @Inject annotation
            for (Field field : classToCheck.getDeclaredFields()) {
                if (field.getAnnotation(Inject.class) != null) {
                    return true;
                }
            }

            // Check all constructors for @Inject annotation
            for (Constructor constructor : classToCheck.getDeclaredConstructors()) {
                if (constructor.getAnnotation(Inject.class) != null) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (needsInjection()) {
            RaceTimerApplication.get(this).inject(this);
        }
    }
}
