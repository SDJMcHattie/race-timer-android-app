package uk.co.iscoding.racetimer.runners;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.common.base.Preconditions;

import java.util.Locale;

import javax.inject.Inject;

import io.realm.Realm;
import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.base.NavFragment;
import uk.co.iscoding.racetimer.nfc.WriteNFCTagActivity;
import uk.co.iscoding.racetimer.realm.model.Runner;

public class RunnerDetailsFragment extends NavFragment {
    private static final String ARG_RUNNER_KEY = "uk.co.iscoding.RUNNER_KEY";

    @Override
    public String getTitle() {
        return getString(R.string.fragment_title_runner_details);
    }

    public static RunnerDetailsFragment newInstance() {
        return newInstance(null);
    }

    public static RunnerDetailsFragment newInstance(String runnerId) {
        RunnerDetailsFragment fragment = new RunnerDetailsFragment();

        Bundle args = new Bundle();
        if (runnerId != null) {
            args.putString(ARG_RUNNER_KEY, runnerId);
        }
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressWarnings("WeakerAccess")
    @Inject Realm realm;

    private Runner runner;
    private boolean isCreatingRunner;
    private EditText firstName;
    private EditText lastName;
    private EditText handicapMinutes;
    private EditText handicapSeconds;
    private boolean changesMade = false;
    private Button saveChanges;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String runnerKey = getArguments().getString(ARG_RUNNER_KEY);
        if (runnerKey != null) {
            runner = realm.where(Runner.class).equalTo(Runner.Fields.PRIMARY_KEY, runnerKey).findFirst();
        }

        isCreatingRunner = runner == null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_runner_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_assign_nfc_tag) {
            startActivity(WriteNFCTagActivity.createIntent(getContext(), runner.getPrimaryKey()));
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_runner_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Gather controls in to variables
        firstName = (EditText) view.findViewById(R.id.edit_first_name);
        lastName = (EditText) view.findViewById(R.id.edit_last_name);
        handicapMinutes = (EditText) view.findViewById(R.id.edit_handicap_minutes);
        handicapSeconds = (EditText) view.findViewById(R.id.edit_handicap_seconds);
        saveChanges = (Button) view.findViewById(R.id.button_save_changes);

        saveChanges.setOnClickListener(saveOnClickListener);

        Button deleteRunner = (Button) view.findViewById(R.id.button_delete);
        deleteRunner.setOnClickListener(deleteOnClickListener);
        deleteRunner.setVisibility(isCreatingRunner ? View.GONE : View.VISIBLE);

        // Start a new Runner object if none was passed in
        if (runner == null) {
            runner = Runner.createInstance();
        }

        populateControls();
        addTextWatchers();
        addFocusListeners();
        updateSaveEnabled();
    }

    private final View.OnClickListener saveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Preconditions.checkNotNull(getHandicapSeconds(), "Handicap seconds should already have been checked for null");

            realm.beginTransaction();

            runner.setFirstName(firstName.getText().toString());
            runner.setLastName(lastName.getText().toString());
            runner.setHandicapSeconds(getHandicapSeconds());

            realm.copyToRealmOrUpdate(runner);
            realm.commitTransaction();

            listener.closeFragment();
        }
    };

    private final View.OnClickListener deleteOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.dialog_title_delete_runner)
                    .setMessage(R.string.dialog_message_delete_runner)
                    .setPositiveButton(R.string.dialog_button_no, null)
                    .setNegativeButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialogInterface, int i) {
                            realm.beginTransaction();
                            runner.deleteFromRealm();
                            realm.commitTransaction();

                            listener.closeFragment();
                        }
                    })
                    .show();
        }
    };

    private void populateControls() {
        firstName.setText(runner.getFirstName());
        lastName.setText(runner.getLastName());
        int seconds = runner.getHandicapSeconds();
        handicapMinutes.setText(String.format(Locale.getDefault(), "%02d", seconds / 60));
        handicapSeconds.setText(String.format(Locale.getDefault(), "%02d", seconds % 60));
    }

    private void addTextWatchers() {
        firstName.addTextChangedListener(new UpdateSaveWatcher());
        lastName.addTextChangedListener(new UpdateSaveWatcher());
        handicapMinutes.addTextChangedListener(new UpdateSaveWatcher());
        handicapSeconds.addTextChangedListener(new UpdateSaveWatcher());
    }

    private void addFocusListeners() {
        handicapMinutes.setOnFocusChangeListener(new HandicapFocusListener());
        handicapSeconds.setOnFocusChangeListener(new HandicapFocusListener());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Override
    public boolean onBackPressed() {
        if (changesMade) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.dialog_title_discard)
                    .setMessage(R.string.dialog_message_discard_runner)
                    .setPositiveButton(R.string.dialog_button_no, null)
                    .setNegativeButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialogInterface, int i) {
                            listener.closeFragment();
                        }
                    })
                    .show();

            return false;
        } else {
            return super.onBackPressed();
        }
    }

    private void updateSaveEnabled() {
        saveChanges.setEnabled(changesMade && getHandicapSeconds() != null && firstName.getText().length() > 0);
    }

    private Integer getHandicapSeconds() {
        int minutes, seconds;
        try {
            minutes = Integer.parseInt(handicapMinutes.getText().toString());
            seconds = Integer.parseInt(handicapSeconds.getText().toString());
        } catch (NumberFormatException ex) {
            return null;
        }

        // Check the ranges on handicap fields
        if (minutes < 0 || minutes > 99 || seconds < 0 || seconds > 59) {
            return null;
        }

        return minutes * 60 + seconds;
    }

    private class UpdateSaveWatcher implements TextWatcher {
        @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {  }
        @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {  }
        @Override public void afterTextChanged(Editable editable) {
            changesMade = true;
            updateSaveEnabled();
        }
    }

    private class HandicapFocusListener implements View.OnFocusChangeListener {
        @Override public void onFocusChange(View view, boolean hasFocus) {
            Preconditions.checkArgument(view instanceof EditText);
            EditText editText = (EditText) view;

            if (!hasFocus) {
                try {
                    int value = Integer.parseInt(editText.getText().toString());
                    editText.setText(String.format(Locale.getDefault(), "%02d", value));
                } catch (NumberFormatException ex) {
                    // Do nothing, we sometime expect the value not to be an integer, so we won't then format it
                }
            }
        }
    }
}
