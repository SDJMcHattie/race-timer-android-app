package uk.co.iscoding.racetimer.realm.model;

import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

import static uk.co.iscoding.racetimer.realm.model.Race.State.BEGUN;
import static uk.co.iscoding.racetimer.realm.model.Race.State.PLANNED;

public class Race extends RealmObject {
    public interface Fields {
        String PRIMARY_KEY = "primaryKey";
        String NAME = "name";
    }

    public enum Type {
        NORMAL,
        HANDICAP,
    }

    public enum State {
        PLANNED,
        BEGUN,
    }

    @PrimaryKey @Required private String primaryKey;
    @Required private String name;
    @Required private Date plannedDate;
    private Date startDate;
    private int typeIndex;

    public String getPrimaryKey() {
        return this.primaryKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getPlannedDate() {
        return plannedDate;
    }

    public void setPlannedDate(Date plannedDate) {
        this.plannedDate = plannedDate;
    }

    private void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public Type getType() {
        return Type.values()[typeIndex];
    }

    public State getState() {
        return startDate == null ? PLANNED : BEGUN;
    }

    public void restartRace(Realm realm, Date startDate) {
        realm.where(Timing.class)
                .equalTo(String.format("%s.%s", Timing.Fields.RACE, Race.Fields.PRIMARY_KEY), getPrimaryKey())
                .findAll()
                .deleteAllFromRealm();

        setStartDate(startDate);
    }

    public Timing getTiming(Realm realm, Runner runner) {
        return realm.where(Timing.class)
                .equalTo(String.format("%s.%s", Timing.Fields.RACE, Race.Fields.PRIMARY_KEY), getPrimaryKey())
                .equalTo(String.format("%s.%s", Timing.Fields.RUNNER, Runner.Fields.PRIMARY_KEY), runner.getPrimaryKey())
                .findFirst();
    }

    public static Race createInstance(Type type) {
        Race race = new Race();
        race.primaryKey = UUID.randomUUID().toString();
        race.typeIndex = type.ordinal();

        return race;
    }
}
