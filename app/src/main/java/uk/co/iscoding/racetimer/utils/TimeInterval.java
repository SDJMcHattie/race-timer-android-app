package uk.co.iscoding.racetimer.utils;

import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TimeInterval {
    public enum Zeroes {
        INCLUDE,
        EXCLUDE,
    }

    public enum SmallestUnit {
        SECONDS,
        TENTHS,
        MILLIS,
    }

    private final long relativeMillis;

    public TimeInterval(Date start, Date end) {
        this.relativeMillis = end.getTime() - start.getTime();
    }

    private int getHours() {
        long relativeTime = relativeMillis;
        long days = TimeUnit.DAYS.convert(relativeTime, TimeUnit.MILLISECONDS);
        relativeTime -= TimeUnit.MILLISECONDS.convert(days, TimeUnit.DAYS);
        return (int)TimeUnit.HOURS.convert(relativeTime, TimeUnit.MILLISECONDS);
    }

    private int getMinutes() {
        long relativeTime = relativeMillis;
        long hours = TimeUnit.HOURS.convert(relativeTime, TimeUnit.MILLISECONDS);
        relativeTime -= TimeUnit.MILLISECONDS.convert(hours, TimeUnit.HOURS);
        return (int)TimeUnit.MINUTES.convert(relativeTime, TimeUnit.MILLISECONDS);
    }

    private int getSeconds() {
        long relativeTime = relativeMillis;
        long mins = TimeUnit.MINUTES.convert(relativeTime, TimeUnit.MILLISECONDS);
        relativeTime -= TimeUnit.MILLISECONDS.convert(mins, TimeUnit.MINUTES);
        return (int)TimeUnit.SECONDS.convert(relativeTime, TimeUnit.MILLISECONDS);
    }

    private int getMillis() {
        long relativeTime = relativeMillis;
        long secs = TimeUnit.SECONDS.convert(relativeTime, TimeUnit.MILLISECONDS);
        relativeTime -= TimeUnit.MILLISECONDS.convert(secs, TimeUnit.SECONDS);
        return (int)relativeTime;
    }

    private int getTenths() {
        return getMillis() / 100;
    }

    public String getFormattedTimeInterval(Zeroes zeroes, SmallestUnit smallestUnit) {
        boolean includeZeros = zeroes == Zeroes.INCLUDE;

        StringBuilder output = new StringBuilder();
        int hours = getHours();
        if (hours > 0 || includeZeros) {
            output.append(String.format(Locale.getDefault(), includeZeros ? "%02d:" : "%d:", hours));
        }

        int minutes = getMinutes();
        if (minutes > 0 || output.length() > 0) {
            output.append(String.format(Locale.getDefault(), output.length() > 0 ? "%02d:" : "%d:", minutes));
        }

        output.append(String.format(Locale.getDefault(), output.length() > 0 ? "%02d" : "%d", getSeconds()));

        switch (smallestUnit) {
            case MILLIS:
                output.append(String.format(Locale.getDefault(), ".%03d", getMillis()));
                break;
            case TENTHS:
                output.append(String.format(Locale.getDefault(), ".%01d", getTenths()));
                break;
        }

        return output.toString();
    }
}
