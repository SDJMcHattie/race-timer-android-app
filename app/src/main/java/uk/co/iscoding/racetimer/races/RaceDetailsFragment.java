package uk.co.iscoding.racetimer.races;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.common.base.Preconditions;

import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import io.realm.Realm;
import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.base.NavFragment;
import uk.co.iscoding.racetimer.realm.model.Race;
import uk.co.iscoding.racetimer.realm.model.Timing;
import uk.co.iscoding.racetimer.utils.TimeInterval;

import static uk.co.iscoding.racetimer.realm.model.Race.State.PLANNED;

public class RaceDetailsFragment extends NavFragment {
    private static final String ARG_RACE_KEY = "uk.co.iscoding.RACE_KEY";

    @Override
    public String getTitle() {
        return race.getName();
    }

    public static RaceDetailsFragment newInstance(String raceId) {
        RaceDetailsFragment fragment = new RaceDetailsFragment();

        Bundle args = new Bundle();
        if (raceId != null) {
            args.putString(ARG_RACE_KEY, raceId);
        }
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressWarnings("WeakerAccess")
    @Inject Realm realm;

    private Button raceStartButton;
    private Button tagRunnersButton;
    private TextView startDateView;
    private View horizontalSeparator;

    private RecyclerView recyclerView;

    private Race race;

    private final Handler raceStartTimeHandler = new Handler();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String raceKey = getArguments().getString(ARG_RACE_KEY);
        Preconditions.checkNotNull(raceKey, "Race Key should not be null to open Race Details");
        race = realm.where(Race.class).equalTo(Race.Fields.PRIMARY_KEY, raceKey).findFirst();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
        raceStartTimeHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_race_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_edit_race) {
            listener.showFragment(EditRaceFragment.newInstance(race.getPrimaryKey()));
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_race_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView raceDate = (TextView)view.findViewById(R.id.race_date);
        TextView raceTime = (TextView)view.findViewById(R.id.race_time);
        raceStartButton = (Button)view.findViewById(R.id.button_start_race);
        tagRunnersButton = (Button)view.findViewById(R.id.button_log_race_runners);
        startDateView = (TextView)view.findViewById(R.id.race_start_time);
        horizontalSeparator = view.findViewById(R.id.horizontal_separator);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        raceDate.setText(DateFormat.format("dd MMM yyyy", race.getPlannedDate()));
        raceTime.setText(DateFormat.format("HH:mm", race.getPlannedDate()));
        raceStartButton.setOnClickListener(raceStartListener);
        tagRunnersButton.setOnClickListener(tagRunnersListener);

        setUpRecyclerView();
        updateViews();
    }

    private void restartRace() {
        realm.beginTransaction();

        race.restartRace(realm, new Date());

        realm.copyToRealmOrUpdate(race);
        realm.commitTransaction();
    }

    private final View.OnClickListener raceStartListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            switch (race.getState()) {
                case PLANNED:
                    // Start the race
                    restartRace();
                    break;
                case BEGUN:
                    // Confirm restart of race
                    new AlertDialog.Builder(getContext())
                            .setTitle(R.string.dialog_title_restart_race)
                            .setMessage(R.string.dialog_message_restart_race)
                            .setPositiveButton(R.string.dialog_button_no, null)
                            .setNegativeButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                                @Override public void onClick(DialogInterface dialogInterface, int i) {
                                    restartRace();
                                }
                            })
                            .show();

                    break;
            }

            updateViews();
        }
    };

    private final View.OnClickListener tagRunnersListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            startActivity(TagRunnersActivity.createIntent(getActivity(), race));
        }
    };

    private void updateViews() {
        updateButtonStates();
        updateStartTime();
    }

    private void updateButtonStates() {
        raceStartButton.setText(race.getState() == PLANNED ? R.string.button_log_race_start : R.string.button_log_race_restart);
        tagRunnersButton.setEnabled(race.getState() != PLANNED && didRaceStartToday());
    }

    private boolean didRaceStartToday() {
        Date currentDate = new Date();
        CharSequence formattedCurrentDate = DateFormat.format("dd MMM yyyy", currentDate);

        Date startDate = race.getStartDate();
        CharSequence formattedStartDate = DateFormat.format("dd MMM yyyy", startDate);

        return formattedCurrentDate.equals(formattedStartDate);
    }

    private String getFormattedStartTimeText() {
        Date startDate = race.getStartDate();

        if (didRaceStartToday()) {
            return getString(R.string.relative_race_start_date_format, new TimeInterval(race.getStartDate(), new Date())
                    .getFormattedTimeInterval(TimeInterval.Zeroes.INCLUDE, TimeInterval.SmallestUnit.SECONDS));
        } else {
            CharSequence dateComponent = DateFormat.format("dd MMM yyyy", startDate);
            CharSequence timeComponent = DateFormat.format("HH:mm", startDate);
            return getString(R.string.absolute_race_start_date_format, dateComponent, timeComponent);
        }
    }

    private void updateStartTimeText() {
        startDateView.setText(getFormattedStartTimeText());

        if (didRaceStartToday()) {
            // Re-run update after a delay
            raceStartTimeHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateStartTimeText();
                }
            }, 1000);
        }
    }

    private void updateStartTime() {
        switch (race.getState()) {
            case PLANNED:
                startDateView.setVisibility(View.GONE);
                horizontalSeparator.setVisibility(View.GONE);
                raceStartTimeHandler.removeCallbacksAndMessages(null);
                break;
            case BEGUN:
                startDateView.setVisibility(View.VISIBLE);
                horizontalSeparator.setVisibility(View.VISIBLE);
                updateStartTimeText();
                break;
        }
    }

    private void setUpRecyclerView() {
        recyclerView.setAdapter(new TimingsAdapter(
                getContext(),
                realm.where(Timing.class)
                        .equalTo(String.format(
                                Locale.getDefault(),
                                "%s.%s", Timing.Fields.RACE, Race.Fields.PRIMARY_KEY
                        ), race.getPrimaryKey())
                        .findAllSortedAsync(Timing.Fields.FINISH_TIME)));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
    }
}
