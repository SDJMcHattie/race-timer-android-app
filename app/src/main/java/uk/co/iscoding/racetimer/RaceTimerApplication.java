package uk.co.iscoding.racetimer;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import dagger.ObjectGraph;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RaceTimerApplication extends Application {
    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();

        objectGraph = ObjectGraph.create(new EntryPointsModule(), new MainModule());

        RealmConfiguration realmConfig = new RealmConfiguration.Builder(this)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfig);

        registerActivityLifecycleCallbacks(new ActivityLifecycleAdapter() {
            @Override
            public void onActivityCreated(Activity a, Bundle savedInstanceState) {
                a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        });
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }

    public static RaceTimerApplication get(Context context) {
        return (RaceTimerApplication)context.getApplicationContext();
    }

    private class ActivityLifecycleAdapter implements ActivityLifecycleCallbacks {
        @Override public void onActivityCreated(Activity activity, Bundle savedInstanceState) {  }
        @Override public void onActivityStarted(Activity activity) {  }
        @Override public void onActivityResumed(Activity activity) {  }
        @Override public void onActivityPaused(Activity activity) {  }
        @Override public void onActivityStopped(Activity activity) {  }
        @Override public void onActivitySaveInstanceState(Activity activity, Bundle outState) {  }
        @Override public void onActivityDestroyed(Activity activity) {  }
    }
}
