package uk.co.iscoding.racetimer.races;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import io.realm.Realm;
import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.base.NavFragment;
import uk.co.iscoding.racetimer.realm.model.Race;

public class RacesFragment extends NavFragment implements RacesAdapter.ActionsListener {
    @Override
    public String getTitle() {
        return getResources().getString(R.string.races_title);
    }

    public static RacesFragment newInstance() {
        return new RacesFragment();
    }

    @SuppressWarnings("WeakerAccess")
    @Inject Realm realm;

    private RecyclerView recyclerView;

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_races, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.showFragment(EditRaceFragment.newInstance());
            }
        });

        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        recyclerView.setAdapter(new RacesAdapter(getContext(), realm.where(Race.class).findAllSortedAsync(Race.Fields.NAME), this));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
    }

    public void onRaceClicked(Race race) {
        listener.showFragment(RaceDetailsFragment.newInstance(race.getPrimaryKey()));
    }
}
