package uk.co.iscoding.racetimer;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import uk.co.iscoding.racetimer.nfc.CheckNFCTagAssignmentFragment;
import uk.co.iscoding.racetimer.races.EditRaceFragment;
import uk.co.iscoding.racetimer.races.RaceDetailsFragment;
import uk.co.iscoding.racetimer.races.RacesFragment;
import uk.co.iscoding.racetimer.races.TagRunnersFragment;
import uk.co.iscoding.racetimer.runners.RunnerDetailsFragment;
import uk.co.iscoding.racetimer.runners.RunnersFragment;

@Module(
        injects = {
                CheckNFCTagAssignmentFragment.class,
                EditRaceFragment.class,
                RaceDetailsFragment.class,
                RacesFragment.class,
                RunnerDetailsFragment.class,
                RunnersFragment.class,
                TagRunnersFragment.class,
        }
)
class MainModule {
    @Provides
    Realm provideRealm() {
        return Realm.getDefaultInstance();
    }
}
