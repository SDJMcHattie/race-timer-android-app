package uk.co.iscoding.racetimer.runners;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.realm.model.Runner;

public class RunnersAdapter extends RealmRecyclerViewAdapter<Runner, RunnersAdapter.RunnerViewHolder> {
    public interface ActionsListener {
        void onRunnerClicked(Runner runner);
    }

    private final ActionsListener actionsListener;

    public RunnersAdapter(@NonNull Context context, @Nullable RealmResults<Runner> data, @NonNull ActionsListener actionsListener) {
        super(context, data, true);
        this.actionsListener = actionsListener;
    }

    @Override
    public RunnerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.rv_item_runner, parent, false);
        return new RunnerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RunnerViewHolder holder, int position) {
        Runner runner = getItem(position);

        if (runner != null) {
            holder.bind(runner, position % 2 == 1, actionsListener);
        }
    }

    static class RunnerViewHolder extends RecyclerView.ViewHolder {
        private final View view;
        private final TextView name;
        private final TextView handicap;

        public RunnerViewHolder(View view) {
            super(view);
            this.view = view;
            name = (TextView) view.findViewById(R.id.runner_name);
            handicap = (TextView) view.findViewById(R.id.runner_handicap);
        }

        public void bind(@NonNull final Runner runner, boolean even, final ActionsListener actionsListener) {
            this.view.setBackground(ContextCompat.getDrawable(
                    view.getContext(),
                    even ? R.drawable.row_background_even : R.drawable.row_background_odd));
            this.view.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View view) {
                    actionsListener.onRunnerClicked(runner);
                }
            });

            name.setText(runner.getCompoundName());
            handicap.setText(getFormattedHandicapTime(runner));
        }

        private String getFormattedHandicapTime(Runner runner) {
            int minutes = runner.getHandicapSeconds() / 60;
            int seconds = runner.getHandicapSeconds() % 60;
            return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        }
    }
}
