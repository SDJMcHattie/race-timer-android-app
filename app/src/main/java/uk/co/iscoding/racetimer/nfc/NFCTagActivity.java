package uk.co.iscoding.racetimer.nfc;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;

import uk.co.iscoding.racetimer.base.FragmentHostActivity;

public abstract class NFCTagActivity extends FragmentHostActivity {
    private NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (nfcAdapter == null) {
            getFragment().setState(WriteNFCTagFragment.State.NFC_UNAVAILABLE);
        } else {
            getFragment().setState(nfcAdapter.isEnabled() ? WriteNFCTagFragment.State.NFC_AVAILABLE : WriteNFCTagFragment.State.NFC_TURNED_OFF);
            nfcAdapter.enableForegroundDispatch(this, getNFCPendingIntent(), getIntentFilters(), null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (nfcAdapter != null) {
            nfcAdapter.disableForegroundDispatch(this);
        }
    }

    private PendingIntent getNFCPendingIntent() {
        return PendingIntent.getActivity(
                this,
                0,
                new Intent(this, getClass())
                        .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP),
                0);
    }

    private IntentFilter[] getIntentFilters() {
        return new IntentFilter[]{ new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED) };
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            getFragment().newNFCTagDiscovered((Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG));
        }
    }

    protected abstract NFCTagFragment getFragment();
}
