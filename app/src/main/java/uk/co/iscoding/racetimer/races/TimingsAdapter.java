package uk.co.iscoding.racetimer.races;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;

import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;
import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.realm.model.Timing;
import uk.co.iscoding.racetimer.utils.TimeInterval;

class TimingsAdapter extends RealmRecyclerViewAdapter<Timing, TimingsAdapter.TimingViewHolder> {
    TimingsAdapter(@NonNull Context context, @Nullable RealmResults<Timing> data) {
        super(context, data, true);
    }

    @Override
    public TimingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.rv_item_timing, parent, false);
        return new TimingViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TimingViewHolder holder, int position) {
        Timing previousTiming = null;
        Timing timing = getItem(position);

        if (position > 0) {
            previousTiming = getItem(position - 1);
        }

        holder.bind(previousTiming, timing, position + 1);
    }

    static class TimingViewHolder extends RecyclerView.ViewHolder {
        private final View view;
        private final TextView positionView;
        private final TextView runnerName;
        private final TextView raceTime;
        private final TextView timeDifference;

        TimingViewHolder(View view) {
            super(view);
            this.view = view;
            positionView = (TextView) view.findViewById(R.id.position);
            runnerName = (TextView) view.findViewById(R.id.runner_name);
            raceTime = (TextView) view.findViewById(R.id.race_time);
            timeDifference = (TextView) view.findViewById(R.id.time_difference);
        }

        void bind(final Timing previousTiming, @NonNull Timing timing, int position) {
            this.view.setBackground(ContextCompat.getDrawable(
                    view.getContext(),
                    position % 2 == 0 ? R.drawable.row_background_even : R.drawable.row_background_odd
            ));

            positionView.setText(String.valueOf(position));
            runnerName.setText(timing.getRunner().getCompoundName());

            raceTime.setText(new TimeInterval(timing.getStartTime(), timing.getFinishTime())
                    .getFormattedTimeInterval(TimeInterval.Zeroes.EXCLUDE, TimeInterval.SmallestUnit.MILLIS));

            if (previousTiming != null) {
                timeDifference.setVisibility(View.VISIBLE);
                TimeInterval timeInterval = new TimeInterval(previousTiming.getFinishTime(), timing.getFinishTime());
                timeDifference.setText(String.format(Locale.getDefault(), "+%s",
                        timeInterval.getFormattedTimeInterval(TimeInterval.Zeroes.EXCLUDE, TimeInterval.SmallestUnit.MILLIS)));
            } else {
                timeDifference.setVisibility(View.GONE);
            }
        }
    }
}
