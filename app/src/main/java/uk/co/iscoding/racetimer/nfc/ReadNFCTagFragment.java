package uk.co.iscoding.racetimer.nfc;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.util.Log;

import java.nio.charset.Charset;
import java.util.Arrays;

import javax.inject.Inject;

import io.realm.Realm;

public abstract class ReadNFCTagFragment extends NFCTagFragment {
    private static final String TAG = ReadNFCTagFragment.class.getSimpleName();

    protected abstract void handleTagMessage(String message);

    @Inject protected Realm realm;

    @Override
    public void onDestroy() {
        super.onDestroy();

        realm.close();
    }

    @Override
    public void newNFCTagDiscovered(Tag detectedTag) {
        readTag(detectedTag);
    }

    private void readTag(Tag tag) {
        NdefMessage message = null;

        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();
                message = ndef.getNdefMessage();
            }
        } catch (Exception e) {
            Log.e(TAG, "Error while trying to read NFC NDEF message.", e);
        }

        if (message != null) {
            handleNDEFMessage(message);
        } else {
            handleTagMessage(null);
        }
    }

    private void handleNDEFMessage(NdefMessage message) {
        NdefRecord record = message.getRecords()[0];
        short tnf = record.getTnf();
        byte[] type = record.getType();
        byte[] data = record.getPayload();

        if (tnf == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(type, NdefRecord.RTD_TEXT)) {
            handleTagMessage(getMessageFromPayload(data));
        } else {
            handleTagMessage(null);
        }
    }

    private String getMessageFromPayload(byte[] data) {
        byte status = data[0];
        int utfBit = status & (1 << 7);
        Charset utfEncoding = (utfBit == 0) ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
        int langByteCount = status - utfBit;
        byte[] textBytes = Arrays.copyOfRange(data, 1 + langByteCount, data.length);
        return new String(textBytes, utfEncoding);
    }
}
