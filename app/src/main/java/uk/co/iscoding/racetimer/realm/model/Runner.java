package uk.co.iscoding.racetimer.realm.model;

import java.util.Locale;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Runner extends RealmObject {
    public interface Fields {
        String PRIMARY_KEY = "primaryKey";
        String COMPOUND_NAME = "compoundName";
    }

    @PrimaryKey @Required private String primaryKey;
    @Required private String firstName;
    private String lastName;
    private String compoundName;
    private int handicapSeconds;

    public String getPrimaryKey() {
        return this.primaryKey;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        updateCompoundName();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        updateCompoundName();
    }

    public int getHandicapSeconds() {
        return handicapSeconds;
    }

    public void setHandicapSeconds(int handicapSeconds) {
        this.handicapSeconds = handicapSeconds;
    }

    public String getCompoundName() {
        return compoundName;
    }

    private void updateCompoundName() {
        if (lastName == null || lastName.length() == 0) {
            this.compoundName = firstName;
        } else {
            this.compoundName = String.format(Locale.getDefault(), "%s, %s", lastName, firstName);
        }
    }

    public static Runner createInstance() {
        Runner runner = new Runner();
        runner.primaryKey = UUID.randomUUID().toString();

        return runner;
    }
}
