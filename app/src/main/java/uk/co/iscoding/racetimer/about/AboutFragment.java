package uk.co.iscoding.racetimer.about;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.base.NavFragment;

public class AboutFragment extends NavFragment {

    @Override
    public String getTitle() {
        return getResources().getString(R.string.about_title);
    }

    public static AboutFragment newInstance() {
        return new AboutFragment();
    }

    private WebView webView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        webView = new WebView(getContext());
        return webView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        webView.loadUrl("file:///android_res/raw/about.html");
    }
}
