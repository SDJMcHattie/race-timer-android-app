package uk.co.iscoding.racetimer.races;

import android.content.Context;
import android.content.Intent;

import uk.co.iscoding.racetimer.nfc.NFCTagActivity;
import uk.co.iscoding.racetimer.nfc.NFCTagFragment;
import uk.co.iscoding.racetimer.realm.model.Race;

public class TagRunnersActivity
        extends NFCTagActivity {
    private static final String RACE_ID_KEY = "uk.co.iscoding.RACE_ID_KEY";

    public static Intent createIntent(Context context, Race race) {
        Intent intent = new Intent(context, TagRunnersActivity.class);
        intent.putExtra(RACE_ID_KEY, race.getPrimaryKey());

        return intent;
    }

    private NFCTagFragment fragment;

    @Override
    protected NFCTagFragment getFragment() {
        if (fragment == null) {
            fragment = TagRunnersFragment.newInstance(getIntent().getStringExtra(RACE_ID_KEY));
        }

        return fragment;
    }
}
