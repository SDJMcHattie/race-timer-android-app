package uk.co.iscoding.racetimer.runners;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import io.realm.Realm;
import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.base.NavFragment;
import uk.co.iscoding.racetimer.nfc.CheckNFCTagAssignmentActivity;
import uk.co.iscoding.racetimer.realm.model.Runner;

public class RunnersFragment extends NavFragment implements RunnersAdapter.ActionsListener {
    @Override
    public String getTitle() {
        return getResources().getString(R.string.runners_title);
    }

    public static RunnersFragment newInstance() {
        return new RunnersFragment();
    }

    private static final int REQUEST_CODE_CHECK_NFC_ASSIGNMENT = 1001;

    @SuppressWarnings("WeakerAccess")
    @Inject Realm realm;

    private RecyclerView recyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_runners, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.showFragment(RunnerDetailsFragment.newInstance());
            }
        });

        setUpRecyclerView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_runners, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_item_check_nfc_tag) {
            startActivityForResult(CheckNFCTagAssignmentActivity.createIntent(getContext()), REQUEST_CODE_CHECK_NFC_ASSIGNMENT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_CHECK_NFC_ASSIGNMENT) {
            if (resultCode == CheckNFCTagAssignmentActivity.RESULT_CODE_RUNNER_FOUND) {
                String runnerId = data.getExtras().getString(CheckNFCTagAssignmentActivity.RESULT_DATA_RUNNER_ID);
                listener.showFragment(RunnerDetailsFragment.newInstance(runnerId));
            }
        }
    }

    private void setUpRecyclerView() {
        recyclerView.setAdapter(new RunnersAdapter(getContext(), realm.where(Runner.class).findAllSortedAsync(Runner.Fields.COMPOUND_NAME), this));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
    }

    public void onRunnerClicked(Runner runner) {
        listener.showFragment(RunnerDetailsFragment.newInstance(runner.getPrimaryKey()));
    }
}
