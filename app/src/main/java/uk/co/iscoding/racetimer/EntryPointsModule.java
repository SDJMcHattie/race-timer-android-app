package uk.co.iscoding.racetimer;

import dagger.Module;

@Module(
        includes = {MainModule.class}
)
class EntryPointsModule {  }
