package uk.co.iscoding.racetimer.nfc;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.Preconditions;

import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.realm.model.Runner;

public class CheckNFCTagAssignmentFragment extends ReadNFCTagFragment {
    public static CheckNFCTagAssignmentFragment newInstance() {
        return new CheckNFCTagAssignmentFragment();
    }

    interface CheckNFCTagAssignmentFragmentHost {
        void finishWithRunnerID(@NonNull String runnerId);
    }

    private final CheckNFCTagAssignmentFragmentHost DUMMY_HOST = new CheckNFCTagAssignmentFragmentHost() {
        @Override public void finishWithRunnerID(@NonNull String runnerId) {  }
    };

    private CheckNFCTagAssignmentFragmentHost host = DUMMY_HOST;

    private TextView instructionsTextView;
    private Button wirelessSettingsButton;
    private ImageView resultImageView;
    private TextView resultErrorMessage;
    private TextView runnersName;
    private Button viewRunnerDetails;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Preconditions.checkArgument(context instanceof CheckNFCTagAssignmentFragmentHost);
        host = (CheckNFCTagAssignmentFragmentHost) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        host = DUMMY_HOST;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_check_nfc_assignment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        instructionsTextView = (TextView) view.findViewById(R.id.nfc_instructions);

        wirelessSettingsButton = (Button) view.findViewById(R.id.button_open_wireless_settings);
        wirelessSettingsButton.setOnClickListener(openWirelessSettingsButtonOnClickListener);

        resultImageView = (ImageView) view.findViewById(R.id.read_result_image_view);
        resultErrorMessage = (TextView) view.findViewById(R.id.read_result_error_message);
        runnersName = (TextView) view.findViewById(R.id.read_result_runner_name);
        viewRunnerDetails = (Button) view.findViewById(R.id.button_view_runner_details);

        updateForCurrentState();
    }

    private final View.OnClickListener openWirelessSettingsButtonOnClickListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
        }
    };

    @Override
    protected void stateWasSet() {
        updateForCurrentState();
    }

    private void updateForCurrentState() {
        if (getView() == null) {
            return;
        }

        switch (getState()) {
            case NFC_UNAVAILABLE:
                wirelessSettingsButton.setVisibility(View.GONE);
                instructionsTextView.setText(R.string.nfc_instructions_no_nfc);
                break;
            case NFC_TURNED_OFF:
                wirelessSettingsButton.setVisibility(View.VISIBLE);
                instructionsTextView.setText(R.string.nfc_instructions_nfc_turned_off);
                break;
            case NFC_AVAILABLE:
                wirelessSettingsButton.setVisibility(View.GONE);
                instructionsTextView.setText(R.string.check_nfc_assignment_instructions);
                break;
        }
    }

    @Override
    protected void handleTagMessage(String message) {
        if (message != null) {
            showResult(realm.where(Runner.class).equalTo(Runner.Fields.PRIMARY_KEY, message).findFirst());
        } else {
            showResult(null);
        }
    }

    private void showResult(final Runner runner) {
        if (runner == null) {
            resultImageView.setImageResource(R.drawable.nfc_tag_failure);

            runnersName.setVisibility(View.GONE);
            viewRunnerDetails.setVisibility(View.GONE);
            resultErrorMessage.setVisibility(View.VISIBLE);
        } else {
            resultImageView.setImageResource(R.drawable.nfc_tag_success);
            runnersName.setText(runner.getCompoundName());
            viewRunnerDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    host.finishWithRunnerID(runner.getPrimaryKey());
                }
            });

            runnersName.setVisibility(View.VISIBLE);
            viewRunnerDetails.setVisibility(View.VISIBLE);
            resultErrorMessage.setVisibility(View.GONE);
        }

        resultImageView.setVisibility(View.VISIBLE);
    }
}
