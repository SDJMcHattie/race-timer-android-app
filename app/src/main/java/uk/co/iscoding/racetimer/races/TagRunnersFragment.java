package uk.co.iscoding.racetimer.races;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.common.base.Preconditions;

import java.util.Date;
import java.util.Locale;

import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.nfc.ReadNFCTagFragment;
import uk.co.iscoding.racetimer.realm.model.Race;
import uk.co.iscoding.racetimer.realm.model.Runner;
import uk.co.iscoding.racetimer.realm.model.Timing;
import uk.co.iscoding.racetimer.utils.TimeInterval;

public class TagRunnersFragment extends ReadNFCTagFragment {
    private static final String RACE_ID_KEY = "uk.co.iscoding.RACE_ID_KEY";

    public static TagRunnersFragment newInstance(String raceId) {
        TagRunnersFragment fragment = new TagRunnersFragment();
        Bundle args = new Bundle();
        args.putString(RACE_ID_KEY, raceId);
        fragment.setArguments(args);

        return fragment;
    }

    private Race race;
    private final Handler raceTimerHandler = new Handler();
    private final Handler scrollHandler = new Handler();

    private TextView timerTextView;
    private TextView nfcErrorTextView;
    private Button wirelessSettingsButton;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String raceId = getArguments().getString(RACE_ID_KEY);
        if (raceId != null) {
            race = realm.where(Race.class)
                    .equalTo(Race.Fields.PRIMARY_KEY, raceId)
                    .findFirst();
        }

        Preconditions.checkNotNull(race);
        getActivity().setTitle(String.format("%s — %s", race.getName(), getString(R.string.tag_runners_activity_title)));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        raceTimerHandler.removeCallbacksAndMessages(null);
        scrollHandler.removeCallbacksAndMessages(null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tag_runners, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        timerTextView = (TextView)view.findViewById(R.id.running_time_view);
        nfcErrorTextView = (TextView)view.findViewById(R.id.nfc_error);
        wirelessSettingsButton = (Button)view.findViewById(R.id.button_open_wireless_settings);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        wirelessSettingsButton.setOnClickListener(openWirelessSettingsButtonOnClickListener);

        updateForCurrentState();
        setUpRecyclerView();
        updateRaceTimer();
    }

    private final View.OnClickListener openWirelessSettingsButtonOnClickListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
        }
    };

    @Override
    protected void stateWasSet() {
        updateForCurrentState();
    }

    private void updateForCurrentState() {
        if (getView() == null) {
            return;
        }

        switch (getState()) {
            case NFC_UNAVAILABLE:
                nfcErrorTextView.setVisibility(View.VISIBLE);
                wirelessSettingsButton.setVisibility(View.GONE);
                nfcErrorTextView.setText(R.string.nfc_instructions_no_nfc);
                break;
            case NFC_TURNED_OFF:
                nfcErrorTextView.setVisibility(View.VISIBLE);
                wirelessSettingsButton.setVisibility(View.VISIBLE);
                nfcErrorTextView.setText(R.string.nfc_instructions_nfc_turned_off);
                break;
            case NFC_AVAILABLE:
                nfcErrorTextView.setVisibility(View.GONE);
                wirelessSettingsButton.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    protected void handleTagMessage(String message) {
        if (message != null) {
            handleRunner(realm.where(Runner.class).equalTo(Runner.Fields.PRIMARY_KEY, message).findFirst());
        }
    }

    private boolean shouldScrollToBottom = false;

    private void handleRunner(Runner runner) {
        if (runner == null) {
            return;
        }

        shouldScrollToBottom = shouldScrollToBottom ||
                layoutManager.findLastCompletelyVisibleItemPosition() == recyclerView.getAdapter().getItemCount() - 1;

        realm.beginTransaction();

        Timing timing = race.getTiming(realm, runner);

        if (timing == null) {
            timing = Timing.createInstance(race, runner);
            timing.setStartTime(race.getStartDate());
        }

        timing.setFinishTime(new Date());

        realm.copyToRealmOrUpdate(timing);
        realm.commitTransaction();

        if (shouldScrollToBottom) {
            // Add a delay before scrolling so that Realm can complete its update
            scrollHandler.removeCallbacksAndMessages(null);
            scrollHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    recyclerView.smoothScrollToPosition(recyclerView.getAdapter().getItemCount() - 1);
                    shouldScrollToBottom = false;
                }
            }, 500);
        }
    }

    private void updateRaceTimer() {
        timerTextView.setText(new TimeInterval(race.getStartDate(), new Date())
                .getFormattedTimeInterval(TimeInterval.Zeroes.INCLUDE, TimeInterval.SmallestUnit.TENTHS));

        raceTimerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateRaceTimer();
            }
        }, 100);
    }

    private void setUpRecyclerView() {
        recyclerView.setAdapter(new TimingsAdapter(
                getContext(),
                realm.where(Timing.class)
                        .equalTo(String.format(
                                Locale.getDefault(),
                                "%s.%s", Timing.Fields.RACE, Race.Fields.PRIMARY_KEY
                        ), race.getPrimaryKey())
                        .findAllSortedAsync(Timing.Fields.FINISH_TIME)));
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }
}
