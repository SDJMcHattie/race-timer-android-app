package uk.co.iscoding.racetimer.nfc;

import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;

import uk.co.iscoding.racetimer.base.BaseFragment;

public abstract class NFCTagFragment extends BaseFragment {
    private final String STATE_KEY = "uk.co.iscoding.STATE_KEY";

    protected enum State {
        NFC_UNAVAILABLE,
        NFC_TURNED_OFF,
        NFC_AVAILABLE,
    }

    private State state;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            state = State.NFC_AVAILABLE;
        } else {
            state = State.values()[savedInstanceState.getInt(STATE_KEY)];
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(STATE_KEY, state.ordinal());
    }

    final void setState(State state) {
        this.state = state;
        stateWasSet();
    }

    protected final State getState() {
        return state;
    }

    protected abstract void stateWasSet();

    public abstract void newNFCTagDiscovered(Tag detectedTag);
}
