package uk.co.iscoding.racetimer.nfc;

import android.content.Context;
import android.content.Intent;

public class WriteNFCTagActivity extends NFCTagActivity {
    public static Intent createIntent(Context context, String tagContents) {
        Intent intent = new Intent(context, WriteNFCTagActivity.class);
        intent.putExtra(EXTRA_TAG_CONTENTS, tagContents);
        return intent;
    }

    private static final String EXTRA_TAG_CONTENTS = "uk.co.iscoding.TAG_CONTENTS";

    private NFCTagFragment fragment;

    @Override
    protected NFCTagFragment getFragment() {
        if (fragment == null) {
            fragment = WriteNFCTagFragment.newInstance(getIntent().getStringExtra(EXTRA_TAG_CONTENTS));
        }

        return fragment;
    }
}
