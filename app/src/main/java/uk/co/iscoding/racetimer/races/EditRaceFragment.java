package uk.co.iscoding.racetimer.races;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

import javax.inject.Inject;

import io.realm.Realm;
import uk.co.iscoding.racetimer.R;
import uk.co.iscoding.racetimer.base.NavFragment;
import uk.co.iscoding.racetimer.realm.model.Race;

public class EditRaceFragment extends NavFragment {
    private static final String ARG_RACE_KEY = "uk.co.iscoding.RACE_KEY";

    @Override
    public String getTitle() {
        return getString(R.string.fragment_title_edit_race);
    }

    public static EditRaceFragment newInstance() {
        return newInstance(null);
    }

    public static EditRaceFragment newInstance(String raceId) {
        EditRaceFragment fragment = new EditRaceFragment();

        Bundle args = new Bundle();
        if (raceId != null) {
            args.putString(ARG_RACE_KEY, raceId);
        }
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressWarnings("WeakerAccess")
    @Inject Realm realm;

    private Race race;
    private boolean isCreatingRace;
    private boolean changesMade = false;
    private Button saveChanges;

    private EditText name;
    private DatePicker datePicker;
    private TimePicker timePicker;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String raceKey = getArguments().getString(ARG_RACE_KEY);
        if (raceKey != null) {
            race = realm.where(Race.class).equalTo(Race.Fields.PRIMARY_KEY, raceKey).findFirst();
        }

        isCreatingRace = race == null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_race, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Gather controls in to variables
        name = (EditText) view.findViewById(R.id.edit_name);
        datePicker = (DatePicker) view.findViewById(R.id.date_picker);
        timePicker = (TimePicker) view.findViewById(R.id.time_picker);
        saveChanges = (Button) view.findViewById(R.id.button_save_changes);

        timePicker.setIs24HourView(true);

        saveChanges.setOnClickListener(saveOnClickListener);

        Button deleteRace = (Button) view.findViewById(R.id.button_delete);
        deleteRace.setOnClickListener(deleteOnClickListener);
        deleteRace.setVisibility(isCreatingRace ? View.GONE : View.VISIBLE);

        Calendar cal = Calendar.getInstance();

        if (race != null) {
            cal.setTime(race.getPlannedDate());
        } else {
            // Start a new Runner object if none was passed in
            race = Race.createInstance(Race.Type.NORMAL);
        }

        datePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), new DatePicker.OnDateChangedListener() {
            @Override public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                changesMade = true;
                updateSaveEnabled();
            }
        });
        setTimePickerInitialValue(timePicker, cal);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                changesMade = true;
                updateSaveEnabled();
            }
        });

        populateControls();
        addTextWatchers();
        updateSaveEnabled();
    }

    @SuppressWarnings("deprecation")
    private void setTimePickerInitialValue(TimePicker timePicker, Calendar cal) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            timePicker.setHour(cal.get(Calendar.HOUR_OF_DAY));
            timePicker.setMinute(cal.get(Calendar.MINUTE));
        } else {
            timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
            timePicker.setCurrentMinute(cal.get(Calendar.MINUTE));
        }
    }

    private final View.OnClickListener saveOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            realm.beginTransaction();

            race.setName(name.getText().toString());
            Calendar cal = Calendar.getInstance();
            cal.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), getHour(timePicker), getMinute(timePicker));
            race.setPlannedDate(cal.getTime());

            realm.copyToRealmOrUpdate(race);
            realm.commitTransaction();

            listener.closeFragment();
        }

        @SuppressWarnings("deprecation")
        private int getHour(TimePicker timePicker) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return timePicker.getHour();
            } else {
                return timePicker.getCurrentHour();
            }
        }

        @SuppressWarnings("deprecation")
        private int getMinute(TimePicker timePicker) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return timePicker.getMinute();
            } else {
                return timePicker.getCurrentMinute();
            }
        }
    };

    private final View.OnClickListener deleteOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.dialog_title_delete_race)
                    .setMessage(R.string.dialog_message_delete_race)
                    .setPositiveButton(R.string.dialog_button_no, null)
                    .setNegativeButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialogInterface, int i) {
                            realm.beginTransaction();
                            race.deleteFromRealm();
                            realm.commitTransaction();

                            listener.closeFragment();
                        }
                    })
                    .show();
        }
    };

    private void populateControls() {
        name.setText(race.getName());
    }

    private void addTextWatchers() {
        name.addTextChangedListener(new UpdateSaveWatcher());
    }

    @Override
    public boolean onBackPressed() {
        if (changesMade) {
            new AlertDialog.Builder(getContext())
                    .setTitle(R.string.dialog_title_discard)
                    .setMessage(R.string.dialog_message_discard_race)
                    .setPositiveButton(R.string.dialog_button_no, null)
                    .setNegativeButton(R.string.dialog_button_yes, new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialogInterface, int i) {
                            listener.closeFragment();
                        }
                    })
                    .show();

            return false;
        } else {
            return super.onBackPressed();
        }
    }

    private void updateSaveEnabled() {
        saveChanges.setEnabled(changesMade && name.getText().length() > 0);
    }

    private class UpdateSaveWatcher implements TextWatcher {
        @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {  }
        @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {  }
        @Override public void afterTextChanged(Editable editable) {
            changesMade = true;
            updateSaveEnabled();
        }
    }
}
