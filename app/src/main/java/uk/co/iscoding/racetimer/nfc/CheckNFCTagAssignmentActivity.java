package uk.co.iscoding.racetimer.nfc;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

public class CheckNFCTagAssignmentActivity
        extends NFCTagActivity
        implements CheckNFCTagAssignmentFragment.CheckNFCTagAssignmentFragmentHost {
    public static Intent createIntent(Context context) {
        return new Intent(context, CheckNFCTagAssignmentActivity.class);
    }

    public static final int RESULT_CODE_RUNNER_FOUND = 2001;
    public static final String RESULT_DATA_RUNNER_ID = "uk.co.iscoding.RUNNER_ID";

    private NFCTagFragment fragment;

    @Override
    protected NFCTagFragment getFragment() {
        if (fragment == null) {
            fragment = CheckNFCTagAssignmentFragment.newInstance();
        }

        return fragment;
    }

    @Override
    public void finishWithRunnerID(@NonNull String runnerId) {
        Intent data = new Intent();
        data.putExtra(RESULT_DATA_RUNNER_ID, runnerId);
        setResult(RESULT_CODE_RUNNER_FOUND, data);
        finish();
    }
}
