package uk.co.iscoding.racetimer.nfc;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Locale;

import uk.co.iscoding.racetimer.R;

public class WriteNFCTagFragment extends NFCTagFragment {
    public static WriteNFCTagFragment newInstance(String tagContents) {
        WriteNFCTagFragment fragment = new WriteNFCTagFragment();

        Bundle args = new Bundle();
        args.putString(ARG_TAG_CONTENTS, tagContents);
        fragment.setArguments(args);

        return fragment;
    }

    private static final String ARG_TAG_CONTENTS = "uk.co.iscoding.TAG_CONTENTS";

    private TextView instructionsTextView;
    private Button wirelessSettingsButton;
    private ImageView resultImageView;
    private TextView resultErrorMessage;

    private String tagContents;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_TAG_CONTENTS)) {
            tagContents = args.getString(ARG_TAG_CONTENTS);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_write_nfc, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        instructionsTextView = (TextView) view.findViewById(R.id.nfc_instructions);

        wirelessSettingsButton = (Button) view.findViewById(R.id.button_open_wireless_settings);
        wirelessSettingsButton.setOnClickListener(openWirelessSettingsButtonOnClickListener);

        resultImageView = (ImageView) view.findViewById(R.id.write_result_image_view);
        resultErrorMessage = (TextView) view.findViewById(R.id.write_result_error_message);

        updateForCurrentState();
    }

    private final View.OnClickListener openWirelessSettingsButtonOnClickListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
        }
    };

    @Override
    protected void stateWasSet() {
        updateForCurrentState();
    }

    public void updateForCurrentState() {
        if (getView() == null) {
            return;
        }

        switch (getState()) {
            case NFC_UNAVAILABLE:
                wirelessSettingsButton.setVisibility(View.GONE);
                instructionsTextView.setText(R.string.nfc_instructions_no_nfc);
                break;
            case NFC_TURNED_OFF:
                wirelessSettingsButton.setVisibility(View.VISIBLE);
                instructionsTextView.setText(R.string.nfc_instructions_nfc_turned_off);
                break;
            case NFC_AVAILABLE:
                wirelessSettingsButton.setVisibility(View.GONE);
                instructionsTextView.setText(R.string.write_nfc_instructions);
                break;
        }
    }

    @Override
    public void newNFCTagDiscovered(Tag detectedTag) {
        @StringRes int errorRes;

        if (areRequiredTechsInList(detectedTag.getTechList())) {
            if (isTagWritable(detectedTag)) {
                errorRes = writeTag(getTagContentsAsNdef(), detectedTag);
            } else {
                errorRes = R.string.nfc_tag_error_not_writable;
            }
        } else {
            errorRes = R.string.nfc_tag_error_no_tech;
        }

        if (errorRes == 0) {
            resultImageView.setImageResource(R.drawable.nfc_tag_success);
            resultErrorMessage.setVisibility(View.GONE);
        } else {
            resultImageView.setImageResource(R.drawable.nfc_tag_failure);
            resultErrorMessage.setText(errorRes);
            resultErrorMessage.setVisibility(View.VISIBLE);
        }

        resultImageView.setVisibility(View.VISIBLE);
    }

    private boolean areRequiredTechsInList(String[] techs) {
        boolean ultralight = false, nfcA = false, ndef = false;

        for (String tech : techs) {
            switch (tech) {
                case "android.nfc.tech.MifareUltralight":
                    ultralight = true;
                    break;
                case "android.nfc.tech.NfcA":
                    nfcA = true;
                    break;
                case "android.nfc.tech.Ndef":
                case "android.nfc.tech.NdefFormatable":
                    ndef = true;
                    break;
            }
        }

        return ultralight && nfcA && ndef;
    }

    private boolean isTagWritable(Tag tag) {
        boolean result = false;

        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();

                if (ndef.isWritable()) {
                    result = true;
                }

                ndef.close();
            }
        } catch (Exception e) {
            // Failed to read tag
        }

        return result;
    }

    private NdefRecord createTextRecord(String payload) {
        byte[] langBytes = Locale.UK.getLanguage().getBytes(Charset.forName("US-ASCII"));
        Charset utfEncoding = Charset.forName("UTF-8");
        byte[] textBytes = payload.getBytes(utfEncoding);
        int utfBit = 0;
        char status = (char) (utfBit + langBytes.length);
        byte[] data = new byte[1 + langBytes.length + textBytes.length];
        data[0] = (byte) status;
        System.arraycopy(langBytes, 0, data, 1, langBytes.length);
        System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);
        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
    }

    private NdefMessage getTagContentsAsNdef() {
        return new NdefMessage(new NdefRecord[] { createTextRecord(tagContents) });
    }

    private @StringRes int writeTag(NdefMessage message, Tag tag) {
        int size = message.toByteArray().length;
        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();

                if (!ndef.isWritable()) {
                    return R.string.nfc_tag_error_read_only;
                }

                if (ndef.getMaxSize() < size) {
                    return R.string.nfc_tag_error_low_capacity;
                }

                ndef.writeNdefMessage(message);
                return 0;
            } else {
                NdefFormatable format = NdefFormatable.get(tag);
                if (format != null) {
                    try {
                        format.connect();
                        format.format(message);
                        return 0;
                    } catch (IOException e) {
                        return R.string.nfc_tag_error_format_failed;
                    }
                } else {
                    return R.string.nfc_tag_error_no_ndef;
                }
            }
        } catch (Exception e) {
            return R.string.nfc_tag_error_write_failed;
        }
    }
}
