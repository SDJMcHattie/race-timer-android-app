package uk.co.iscoding.racetimer.base;

import android.content.Context;
import android.view.MenuItem;

import com.google.common.base.Preconditions;

public class NavFragment extends BaseFragment {
    public interface Listener {
        void showFragment(NavFragment fragment);
        void closeFragment();
    }

    public String getTitle() {
        return "";
    }

    private MenuItem navMenuItem;
    protected NavFragment.Listener listener;

    public MenuItem getNavMenuItem() {
        return navMenuItem;
    }

    public void setNavMenuItem(MenuItem item) {
        navMenuItem = item;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Preconditions.checkArgument(context instanceof NavFragment.Listener, "Supplied context must be a NavFragment.Listener");
        this.listener = (NavFragment.Listener) context;
    }

    public boolean onBackPressed() {
        return true;
    }
}
